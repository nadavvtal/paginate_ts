import * as yup from "yup";

export const schema = yup.object().shape({
  first_name: yup.string().required(),
  last_name: yup.string().required(),
  age: yup.number().required().positive().integer(),
  email: yup.string().email(),
  phone: yup.string.phone()
});

