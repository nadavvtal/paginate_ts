import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./routers/user.router.js";
import {printError,errorResponse,not_found} from "./middleware/errors.handler.js";   
import {generateUuid} from "./middleware/generateUuid.js";
import { errorsLogger } from "./loggers/errorLogers.js";
import { httpLoger } from "./loggers/httpLoggers.js";


class MyApp {
    PORT = "";
    HOST = "";
    DB_URI = "";
    app: express.Application;

    constructor(){
        this.initializeEnv();
        this.app = this.initializeApp();
        this.initializeHttpLogger();
        this.initializeMiddlwears();
        this.initializeRouters();
        this.initializeErrorHandling();
        this.connectDatabase();
        this.connectServer();
    }
    initializeEnv(){
        const { PORT = 8080, HOST = "localhost", DB_URI ="mongodb://localhost:27017/crud-demo" } = process.env;
        this.PORT =String(PORT);
        this.HOST = HOST;
        this.DB_URI = DB_URI;
    }
    initializeApp(){
        return express();
    }

    initializeHttpLogger(){
        const HTTP_LOG_FILE_PATH = "./logs/httpLogs.log";
        const httpMiddleWareLogger = httpLoger(HTTP_LOG_FILE_PATH);
        this.app.use(httpMiddleWareLogger);
    }
    initializeMiddlwears(){
        this.app.use(generateUuid);
        this.app.use(cors());
        this.app.use(morgan("dev"));
    }

    initializeRouters(){
        this.app.use("/api/users", user_router);
        this.app.use("*", not_found);
    }

    initializeErrorHandling(){
        // central error handling
        const ERRORS_LOG_FILE_PATH = "./logs/errorLogs.log";
        this.app.use(printError);
        this.app.use( errorsLogger(ERRORS_LOG_FILE_PATH));
        this.app.use(errorResponse);
        
    }
    async connectDatabase(){
        await connect_db(String(this.DB_URI));
    }

    async connectServer(){
        await this.app.listen(Number(this.PORT), this.HOST);
        log.magenta("api is live on", ` ✨ ⚡  http://${this.PORT}:${this.HOST} ✨ ⚡`);

    }
}
const myApp = new MyApp();
