import userModel from "../models/user.model.js";
import { Request, Response, NextFunction } from "express";
class UserController {
    async addUser(req: Request, res: Response, next: NextFunction) {
        const user = await userModel.create(req.body);
        res.status(200).json(user);
    }

    async getAllUsers(req: Request, res: Response, next: NextFunction) {
        console.log("in get all worksssss");
        const users = await // .select(`-__v`);
        userModel.find().select(`-_id 
                                            first_name 
                                            last_name 
                                            email 
                                            phone`);
        res.status(200).json(users);
    }

    async getUser(req: Request, res: Response, next: NextFunction) {
        const user = await userModel.findById(req.params.id);
        // .select(`-_id
        //     first_name
        //     last_name
        //     email
        //     phone`);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    }

    async updateUser(req: Request, res: Response, next: NextFunction) {
        const user = await userModel.findByIdAndUpdate(
            req.params.id,
            req.body,
            { new: true, upsert: false }
        );
        res.status(200).json(user);
    }

    async deleteUser(req: Request, res: Response, next: NextFunction) {
        const user = await userModel.findByIdAndRemove(req.params.id);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    }
}

export const userController = new UserController();
