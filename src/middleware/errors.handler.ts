import { Request, Response, NextFunction } from "express";
import {
    HttpException,
    UrlNotFoundException,
} from "../exceptions/exceptions.list.js";

// eslint-disable-next-line no-undef
const { NODE_ENV } = process.env;

export const printError = (
    err: HttpException,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    console.log(err.message);
    next(err);
};

export const errorResponse = (
    err: HttpException,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (NODE_ENV !== "production")
        res.status(500).json({ status: err.message, stack: err.stack });
    else res.status(500).json({ status: err.message });
};

export const not_found = (req: Request, res: Response, next: NextFunction) => {
    next(new UrlNotFoundException(req.url));
};
