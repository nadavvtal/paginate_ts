import { Request, Response, NextFunction } from "express";
import pkg from "uuid";
const { v4: uuid } = pkg;

export const generateUuid = function (
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const uniqIdentifier: string = uuid();
        req.uuid = uniqIdentifier;
        next();
    } catch (err) {
        next(err);
    }
};
