import { Request, Response, NextFunction } from "express";
import userModel from "../models/user.model.js";
import { UserNotExistsException } from "../exceptions/exceptions.list.js";

export const idExist = function (
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        if (userModel.find({ UserId: req.params.id }).count() > 0) {
            next();
        } else {
            throw new UserNotExistsException(req.params.id);
        }
    } catch (err) {
        next(err);
    }
};
