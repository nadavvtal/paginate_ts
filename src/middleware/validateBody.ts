import * as yup from "yup";
import { Request, Response, NextFunction } from "express";

const yup_validator = (req: Request, res: Response, next: NextFunction) => {
    const phoneRegExp =
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    const schema = yup.object().shape({
        first_name: yup.string().required(),
        last_name: yup.string().required(),
        email: yup.string().email().required(),
        phone: yup.string().matches(phoneRegExp),
    });
    schema
        .validate(req.body)
        .then((valid) => next())
        .catch((err) => next(err));
};
export default yup_validator;
