import express from "express";
import raw from "../wrappers/route.async.wrapper.js";
import user_model from "../models/user.model.js";
import { Request, Response, NextFunction } from "express";

const pagination_router = express.Router();
pagination_router.use(express.json());

pagination_router.get(
    "/:page/:resultsNumber",
    raw(async (req: Request, res: Response) => {
        console.log("g");
        const user = await user_model
            .find()
            .skip(Number(req.params.page) * Number(req.params.resultsNumber))
            .limit(Number(req.params.resultsNumber));

        res.status(200).json(user);
    })
);
export default pagination_router;
