import raw from "../wrappers/route.async.wrapper.js";
import express from "express";
import pagination_router from "./pagination.router.js";
import yup_validator from "../middleware/validateBody.js";
import { userController } from "../controllers/user.controller.js";
import { idExist } from "../middleware/idExist.js";

const router = express.Router();
router.use(express.json());
router.use("/pagination", pagination_router);

// CREATES A NEW USER
router.post("/", yup_validator, raw(userController.addUser));

// GET ALL USERS
router.get("/", raw(userController.getAllUsers));

// GETS A SINGLE USER
router.get("/:id", idExist, raw(userController.getUser));

// UPDATES A SINGLE USER
router.put("/:id", idExist, yup_validator, raw(userController.updateUser));

// DELETES A USER
router.delete("/:id", idExist, raw(userController.deleteUser));

export default router;
