class HttpException extends Error {
    constructor(public status: number, public message: string) {
        super(message);
    }
}

class internalException extends HttpException {
    constructor(action: string) {
        super(500, "DB Internal error in proccess of" + action);
    }
}

class UrlNotFoundException extends HttpException {
    constructor(path: string) {
        super(404, "Url " + path + " not found");
    }
}
class UserNotExistsException extends HttpException {
    constructor(id: string) {
        super(400, `There isnt such user with id: ${id}`);
    }
}

export {
    internalException,
    HttpException,
    UrlNotFoundException,
    UserNotExistsException,
};
