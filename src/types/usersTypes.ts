export interface User {
    id: number | string;
    gender: string;
    age: number | string;
    description: string;
}

export interface DataBase {
    users: User[];
    currentId: number;
}

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Express {
        export interface Request {
            uuid: string;
        }
    }
}
