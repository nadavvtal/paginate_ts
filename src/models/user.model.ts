import mongoose from "mongoose";
const { Schema, model } = mongoose;
const UserSchema = new Schema({
    first_name: {
        type: String,
        required: [true, "First name required"],
        minlength: [2, "First name must be at least 2 characters."],
    },
    last_name: {
        type: String,
        required: [true, "Last name required"],
        minlength: [2, "Last name must be at least 2 characters"],
    },
    email: {
        type: String,
        required: [true, "email required"],
        unique: true,
        match: [
            /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
            "email address not valid",
        ],
    },
    phone: {
        type: String,
        required: true,
        match: [
            /^[0-9]{3}[-s.][0-9]{3}[-s.][0-9]{4}$/,
            "Phone must be in validate form.",
        ],
    },
});
export default model("user", UserSchema);
