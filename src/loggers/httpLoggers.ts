import fs from "fs";
import { Response, Request, NextFunction } from "express";

export function httpLoger(path: string) {
    return (req: Request, res: Response, next: NextFunction) => {
        const log = `${req.method} => ${req.url} => ${Date.now()} =>uuid:  ${
            req.uuid
        }\n`;
        fs.appendFile(path, log, (err) => {
            if (err) {
                console.log(err);
                next(err);
            } else {
                next();
            }
        });
    };
}
