import fs from "fs";
import { Response, Request, NextFunction } from "express";

export function errorsLogger(path: string) {
    return (error: Error, req: Request, res: Response, next: NextFunction) => {
        const log = `uuid: ${req.uuid} message: ${error.message} >> stack: ${error.stack} \n`;
        fs.appendFile(path, log, (err) => {
            if (err) {
                console.log(err);
                next(error);
            } else {
                next(error);
            }
        });
    };
}
