import mongoose from "mongoose";
import log from "@ajar/marker";

export const connect_db = async (uri: string) => {
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    };
    console.log("this uri", uri);
    await mongoose.connect(uri, options);
    log.magenta(" ✨  Connected to Mongo DB ✨ ");
};
